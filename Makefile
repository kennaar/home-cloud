up: | up-portainer up-blocky up-omada up-cloudflare-ddns up-traefik up-apps
	echo "Starting all services"
.PHONY: up

stop: | stop-apps stop-traefik stop-cloudflare-ddns stop-omada stop-portainer
	echo "Stopping all services (except blocky)"
.PHONY: stop

up-portainer:
	docker compose --project-directory=portainer up --force-recreate -d
.PHONY: up-portainer

stop-portainer:
	docker compose --project-directory=portainer stop
.PHONY: stop-portainer

up-blocky:
	./blocky/generate_dynamic_config.sh
	docker compose --project-directory=blocky up --force-recreate -d
.PHONY: up-blocky

stop-blocky:
	docker compose --project-directory=blocky stop
.PHONY: stop-blocky

up-omada:
	docker compose --project-directory=omada-controller up --force-recreate -d
.PHONY: up-omada

stop-omada:
	docker compose --project-directory=omada-controller stop
.PHONY: stop-omada

up-cloudflare-ddns:
	docker compose --project-directory=cloudflare-ddns up --force-recreate -d
.PHONY: up-cloudflare-ddns

stop-cloudflare-ddns:
	docker compose --project-directory=cloudflare-ddns stop
.PHONY: stop-cloudflare-ddns

up-grafana:
	docker compose --project-directory=grafana up --force-recreate -d
.PHONY: up-grafana

stop-grafana:
	docker compose --project-directory=grafana stop
.PHONY: stop-grafana

up-traefik:
	docker compose --project-directory=traefik up --force-recreate -d
.PHONY: up-traefik

stop-traefik:
	docker compose --project-directory=traefik stop
.PHONY: stop-traefik

up-apps: up-miniflux up-vaultwarden up-hass up-paperless
	echo "Starting all non-essential services"
.PHONY: up-apps

stop-apps: stop-miniflux stop-vaultwarden stop-hass stop-paperless
	echo "Stopping all non-essential services"
.PHONY: stop-apps

up-miniflux:
	docker compose --project-directory=miniflux up --force-recreate -d
.PHONY: up-miniflux

stop-miniflux:
	docker compose --project-directory=miniflux stop
.PHONY: stop-miniflux

up-paperless:
	docker compose --project-directory=paperless up --force-recreate -d
.PHONY: up-paperless

stop-paperless:
	docker compose --project-directory=paperless stop
.PHONY: stop-paperless

up-vaultwarden:
	docker compose --project-directory=vaultwarden up --force-recreate -d
.PHONY: up-vaultwarden

stop-vaultwarden:
	docker compose --project-directory=vaultwarden stop
.PHONY: stop-vaultwarden

up-hass:
	docker compose --project-directory=hass up --force-recreate -d
.PHONY: up-hass

stop-hass:
	docker compose --project-directory=hass stop
.PHONY: stop-hass
