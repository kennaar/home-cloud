#!/usr/bin/env bash

set -eux

SCRIPT_DIR="$(dirname -- "$(readlink -f "${BASH_SOURCE[0]}")")"

REQUIRED_VARIABLES="TRAEFIK_BASE_DOMAIN_NAME|TRAEFIK_IP_ADDRESS|BLOCKY_QUERYLOG_DB_URL|ROUTER_IP|ROUTER_DNS_DOMAIN"

# Loop through each variable and check if it is set
for var in $(echo $REQUIRED_VARIABLES | tr '|' ' '); do
    if [ -z "${!var}" ]; then
        echo "$var is empty or unset"
        exit 1
    fi
done

cat <<EOF > "$SCRIPT_DIR/config/custom_dns.yaml"
customDNS:
  customTTL: 1h
  filterUnmappedTypes: true
  mapping:
    $TRAEFIK_BASE_DOMAIN_NAME: $TRAEFIK_IP_ADDRESS
EOF

cat <<EOF > "$SCRIPT_DIR/config/conditional.yaml"
conditional:
  mapping:
    $ROUTER_DNS_DOMAIN: $ROUTER_IP
EOF

cat <<EOF > "$SCRIPT_DIR/config/client_lookup.yaml"
clientLookup:
  # optional: this DNS resolver will be used to perform reverse DNS lookup (typically local router)
  upstream: $ROUTER_IP
  # optional: some routers return multiple names for client (host name and user defined name). Define which single name should be used.
  singleNameOrder:
    - 2
    - 1
EOF

cat <<EOF > "$SCRIPT_DIR/config/query_log.yaml"
queryLog:
  type: postgresql
  target: '$BLOCKY_QUERYLOG_DB_URL'
  logRetentionDays: 60
EOF