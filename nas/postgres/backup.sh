#!/bin/bash

set -eux

trap "break;exit" SIGHUP SIGINT SIGTERM

sleep 2m

while /bin/true; do
  pg_dumpall > /dump/dump_"$(date '+%d-%m-%Y_%H:%M:%S')".psql

  # List all files (newest first), keep first X lines + list all files
  # Sort all lines, remove duplicates
  # Remove resulting files
  # -> Newest X amount of files are kept
  ls /dump/dump_*.psql | sort -r | uniq | tail -n +"$((BACKUP_NUMBER_TO_KEEP + 1))" | xargs rm -f

  sleep "$BACKUP_FREQUENCY"
done
